public interface IWebFetcher
{
    Task<string> FetchHtmlAsync(string url);
}

