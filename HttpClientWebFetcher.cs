public class HttpClientWebFetcher : IWebFetcher
{
    private readonly HttpClient _client;

    public HttpClientWebFetcher()
    {
        _client = new HttpClient();
    }

    public async Task<string> FetchHtmlAsync(string url)
    {
        var response = await _client.GetAsync(url);
        
        if(!response.IsSuccessStatusCode)
        {
            throw new Exception($"Failed to fetch the website. Status: {response.StatusCode}");
        }
        
        return await response.Content.ReadAsStringAsync();
    }
}
