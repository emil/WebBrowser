public class ConsoleInputService : IInputService
{
    public string GetUserInput()
    {
        return Console.ReadLine();
    }
}

