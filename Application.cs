public class Application
{
    private readonly IInputService _inputService;
    private readonly IWebFetcher _webFetcher;

    public Application(IInputService inputService, IWebFetcher webFetcher)
    {
        _inputService = inputService;
        _webFetcher = webFetcher;
    }

    public async Task RunAsync()
    {
        Console.WriteLine("Enter a website URL:");
        var url = _inputService.GetUserInput();
        
        try
        {
            var html = await _webFetcher.FetchHtmlAsync(url);
            Console.WriteLine(html);
        }
        catch(Exception ex)
        {
            Console.WriteLine($"Error fetching the website: {ex.Message}");
        }
    }
}

