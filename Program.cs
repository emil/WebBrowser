﻿class Program
{
    static async Task Main(string[] args)
    {
        IInputService inputService = new ConsoleInputService();
        IWebFetcher webFetcher = new HttpClientWebFetcher();

        var app = new Application(inputService, webFetcher);
        await app.RunAsync();
    }
}
